module.exports = function findPlayerWithMostManOfmatchAwards(matchesData) {
  const playerAwardsBySeason = {};

  for (const match of matchesData) {
    const season = match.season;
    const playerName = match.player_of_match;

    if (!playerAwardsBySeason[season]) {
      playerAwardsBySeason[season] = {};
    }

    if (playerAwardsBySeason[season][playerName]) {
      playerAwardsBySeason[season][playerName]++;
    } else {
      playerAwardsBySeason[season][playerName] = 1;
    }
  }

  const highestAwardsBySeason = {};

  for (const season in playerAwardsBySeason) {
    const playersInSeason = playerAwardsBySeason[season];
    const maxAwards = Math.max(...Object.values(playersInSeason));

    const highestAwardsPlayers = Object.keys(playersInSeason).filter(
      (player) => playersInSeason[player] === maxAwards,
    );

    highestAwardsBySeason[season] = highestAwardsPlayers;
  }

  return highestAwardsBySeason;
};
