module.exports = function FindTeamsWhoWonTossAlsoWonMatch(matchesData) {
  const teamsWhoWonTossAndMatch = {};

  for (const match of matchesData) {
    const tossWinner = match.toss_winner;
    const matchWinner = match.winner;

    if (tossWinner === matchWinner) {
      if (teamsWhoWonTossAndMatch[tossWinner]) {
        teamsWhoWonTossAndMatch[tossWinner]++;
      } else {
        teamsWhoWonTossAndMatch[tossWinner] = 1;
      }
    }
  }

  return teamsWhoWonTossAndMatch;
};
