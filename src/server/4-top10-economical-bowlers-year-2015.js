module.exports = function mostEconomicBowlers(
  matchesData,
  deliveriesData,
  year,
  numberOfBowlers,
) {
  const bowlerStats = {};

  // Filter matches in the specified year
  const matchesInYear = matchesData.filter((match) => match.season === year);

  // Calculate stats of bowlers
  for (const delivery of deliveriesData) {
    const matchId = delivery.match_id;
    const bowler = delivery.bowler;
    const runs = Number(delivery.total_runs);

    // The some() method checks if any array elements pass a test (provided as a callback function).
    if (matchesInYear.some((match) => match.id === matchId)) {
      // If bowler is new
      if (!bowlerStats[bowler]) {
        bowlerStats[bowler] = { runs: 0, balls: 0 };
      }
      // else if already have stats
      bowlerStats[bowler].runs += runs;
      bowlerStats[bowler].balls++;
    }
  }

  // Calculate economy and sort bowlers by economy
  const topEconomicalBowlers = Object.entries(bowlerStats)
    .map(([bowler, stats]) => ({
      bowler,
      economy: (stats.runs / (stats.balls / 6)).toFixed(2),
    }))
    .sort((a, b) => parseFloat(a.economy) - parseFloat(b.economy)) // Sort as floating-point numbers
    .slice(0, numberOfBowlers)
    .reduce((result, bowlerObj) => {
      result[bowlerObj.bowler] = {
        economy: bowlerObj.economy,
        runs: bowlerStats[bowlerObj.bowler].runs,
        balls: bowlerStats[bowlerObj.bowler].balls,
      };

      return result;
    }, {});

  return topEconomicalBowlers;
};
