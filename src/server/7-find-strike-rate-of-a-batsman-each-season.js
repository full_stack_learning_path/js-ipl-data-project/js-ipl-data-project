module.exports = function findStrikeRate(matchesData, deliveriesData) {
  const strikeRateBySeason = {};

  for (const delivery of deliveriesData) {
    const matchId = delivery.match_id;
    const batsman = delivery.batsman;
    const runs = parseInt(delivery.total_runs, 10);
    const balls = parseInt(delivery.ball, 10);

    // Find the match in matchesData
    const match = matchesData.find((match) => match.id === matchId);

    if (match) {
      const season = match.season;

      // Initialise seasons strike rate if it does not exists
      if (!strikeRateBySeason[season]) {
        strikeRateBySeason[season] = {};
      }

      // Initialise the batsman's stats for the season if not exists
      if (!strikeRateBySeason[season][batsman]) {
        strikeRateBySeason[season][batsman] = {
          runs: 0,
          balls: 0,
          strikeRate: 0,
        };
      }

      // Update stats for the season
      strikeRateBySeason[season][batsman].runs += runs;
      strikeRateBySeason[season][batsman].balls += balls;
      strikeRateBySeason[season][batsman].strikeRate = (
        (strikeRateBySeason[season][batsman].runs /
          strikeRateBySeason[season][batsman].balls) *
        100
      ).toFixed(2);
    }
  }

  return strikeRateBySeason;
};
