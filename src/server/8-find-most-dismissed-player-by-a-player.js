module.exports = function findMostDismissedBatsman(deliveries) {
  const mostDismissedBatsman = {};

  let maxDismissalCount = 0;
  let maxDismissedBatsman = null;
  let maxDismissalsByBowler = [];

  deliveries.forEach((delivery) => {
    const { dismissal_kind, batsman, bowler } = delivery;

    if (dismissal_kind && dismissal_kind !== 'run out') {
      mostDismissedBatsman[batsman] = mostDismissedBatsman[batsman] || {};
      mostDismissedBatsman[batsman][bowler] =
        (mostDismissedBatsman[batsman][bowler] || 0) + 1;

      const currentDismissalCount = mostDismissedBatsman[batsman][bowler];

      if (currentDismissalCount > maxDismissalCount) {
        maxDismissalCount = currentDismissalCount;
        maxDismissedBatsman = batsman;
        maxDismissalsByBowler = [bowler];
      } else if (currentDismissalCount === maxDismissalCount) {
        maxDismissalsByBowler.push(bowler);
      }
    }
  });

  return {
    batsman: maxDismissedBatsman,
    maxDismissalsByBowler: maxDismissalsByBowler,
    maxDismissals: maxDismissalCount,
  };
};
