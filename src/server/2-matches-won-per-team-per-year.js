module.exports = function matchesWonPerTeamPerYear(matchesData) {
  const matchesWonPerTeamPerYear = {};

  for (const match of matchesData) {
    const year = match['season'];
    const winner = match['winner'];

    if (!matchesWonPerTeamPerYear[year]) {
      matchesWonPerTeamPerYear[year] = {};
    }

    if (matchesWonPerTeamPerYear[year][winner]) {
      matchesWonPerTeamPerYear[year][winner]++;
    } else {
      matchesWonPerTeamPerYear[year][winner] = 1;
    }
  }

  return matchesWonPerTeamPerYear;
};
