module.exports = function findMostEconomicalSuperoversBowler(deliveriesData) {
  const bowlerStats = {};

  for (const delivery of deliveriesData) {
    if (delivery.is_super_over === '1') {
      const bowler = delivery.bowler;
      const runs = Number(delivery.total_runs);

      if (!bowlerStats[bowler]) {
        bowlerStats[bowler] = {
          runs: 0,
          balls: 0,
        };
      }

      bowlerStats[bowler].runs += runs;
      bowlerStats[bowler].balls++;
    }
  }

  let bestEconomy = Infinity;
  let bestEconomyBowler = [];

  for (const bowler in bowlerStats) {
    const economy = (
      bowlerStats[bowler].runs /
      (bowlerStats[bowler].balls / 6)
    ).toFixed(2);

    if (economy < bestEconomy) {
      bestEconomy = economy;
      bestEconomyBowler = [bowler];
    } else if (bestEconomy == economy) {
      bestEconomy.push(bowler);
    }
  }

  return {
    bowler: bestEconomyBowler,
    economy: parseFloat(bestEconomy),
  };
};
