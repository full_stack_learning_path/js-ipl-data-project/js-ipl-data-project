module.exports = function extraRunsConcededPerTeamInYear(
  matchesData,
  deliveriesData,
  targetYear,
) {
  const extraRunsPerTeam = {};

  // Create a dictionary to map match IDs to corresponding season
  const matchIdToSeason = {};
  for (const match of matchesData) {
    matchIdToSeason[match['id']] = match['season'];
  }

  // Calculate extra runs per team for the target year
  for (const delivery of deliveriesData) {
    const matchId = delivery['match_id'];
    const season = matchIdToSeason[matchId];

    if (season === targetYear) {
      const bowlingTeam = delivery['bowling_team'];
      const extraRuns = parseInt(delivery['extra_runs'], 10);

      if (extraRunsPerTeam[bowlingTeam]) {
        extraRunsPerTeam[bowlingTeam] += extraRuns;
      } else {
        extraRunsPerTeam[bowlingTeam] = extraRuns;
      }
    }
  }

  return extraRunsPerTeam;
};
