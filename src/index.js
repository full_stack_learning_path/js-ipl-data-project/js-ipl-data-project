const fs = require('fs');
const csv = require('csv-parser');

const matchesPerYear = require('../src/server/1-matches-per-year');
const matchesWonPerTeamPerYear = require('../src/server/2-matches-won-per-team-per-year');
const extraRunsConcededPerTeamInYear = require('../src/server/3-extra-runs-conceded-per-team-year-2016');
const mostEconomicBowlersInYear = require('../src/server/4-top10-economical-bowlers-year-2015');
const teamsWhoWonTossAndMatch = require('../src/server/5-find-teams-who-won-toss-won-match');
const findPlayerWithMostManOfmatchAwards = require('../src/server/6-find-player-with-most-man-of-match-awards');
const findStrikeRateBySeason = require('./server/7-find-strike-rate-of-a-batsman-each-season');
const findMostDismissedBatsman = require('./server/8-find-most-dismissed-player-by-a-player');
const findMostEconomicBowler = require('./server/9-find-most-economic-super-overs-bowler');

// Function to read CSV file
async function readCSVFile(filePath) {
  return new Promise((resolve, reject) => {
    const data = [];
    fs.createReadStream(filePath)
      .pipe(csv())
      .on('data', (row) => {
        data.push(row);
      })
      .on('end', () => {
        resolve(data);
      })
      .on('error', (error) => {
        reject(error);
      });
  });
}

// Main function to run all tasks
async function runTasks() {
  try {
    const matchesData = await readCSVFile('./data/matches.csv');
    const deliveriesData = await readCSVFile('./data/deliveries.csv');

    // Task 1: Matches per Year
    const matchesPerYearResult = matchesPerYear(matchesData);
    fs.writeFileSync(
      './public/output/matchesPerYear.json',
      JSON.stringify(matchesPerYearResult, null, 2),
    );
    console.log('1- Matches per year written to file matchesPerYear.json');

    // Task 2: Matches Won per Team per Year
    const matchesWonPerTeamPerYearResult =
      matchesWonPerTeamPerYear(matchesData);
    fs.writeFileSync(
      './public/output/matchesWonPerTeamPerYear.json',
      JSON.stringify(matchesWonPerTeamPerYearResult, null, 2),
    );
    console.log(
      '2- Matches won per team per year written to file matchesWonPerTeamPerYear.json',
    );

    // Task 3: Extra Runs Conceded per Team in 2016
    const extraRunsPerTeamResult = extraRunsConcededPerTeamInYear(
      matchesData,
      deliveriesData,
      '2016',
    );
    fs.writeFileSync(
      './public/output/extraRunsPerTeamIn2016.json',
      JSON.stringify(extraRunsPerTeamResult, null, 2),
    );
    console.log(
      '3- Extra runs per team in 2016 written to file extraRunsPerTeamIn2016.json',
    );

    // Task 4: Top 10 most economical bowlers in year 2015
    const targetYear = '2015';
    const numberOfTopBowlers = 10;

    const top10EconomicalBowlers = mostEconomicBowlersInYear(
      matchesData,
      deliveriesData,
      targetYear,
      numberOfTopBowlers,
    );
    fs.writeFileSync(
      './public/output/top10MostEconomicBowlers2015.json',
      JSON.stringify(top10EconomicalBowlers, null, 2),
    );
    console.log(
      '4- Top 10 most economical ballers in 2015 written to file top10MostEconomicBowlers2015.json',
    );

    // Task 5: Find the number of times each team won the toss and also won the match
    const teamsWhoWonTossAndMatchBoth = teamsWhoWonTossAndMatch(matchesData);
    fs.writeFileSync(
      './public/output/teamsWhoWonTossWonMatch.json',
      JSON.stringify(teamsWhoWonTossAndMatchBoth, null, 2),
    );
    console.log(
      '5- Teams who won toss also went to won match written to file teamsWhoWonTossWonMatch.json',
    );

    // Task 6: Find a player who has won the highest number of Player of the Match awards for each season
    const playerWithMostAwards =
      findPlayerWithMostManOfmatchAwards(matchesData);
    fs.writeFileSync(
      './public/output/playerWithMostManOfMatchAwards.json',
      JSON.stringify(playerWithMostAwards, null, 2),
    );
    console.log(
      '6- Player with most man of match awards written to file playerWithMostManOfAwards.json',
    );

    // Task 7: Find the strike rate of a batsman for each season
    const strikeRateBySeason = findStrikeRateBySeason(
      matchesData,
      deliveriesData,
    );
    fs.writeFileSync(
      './public/output/strikeRateOfBatsmanForEachSeason.json',
      JSON.stringify(strikeRateBySeason, null, 2),
    );
    console.log(
      '7- Strike rate of a batsman for each season written to file strikerateOfBatsmanForEachSeason.json',
    );

    // Task 8: Find the highest number of times one player has been dismissed by another player
    const mostDismissedPlayer = findMostDismissedBatsman(deliveriesData);
    fs.writeFileSync(
      './public/output/mostDismissedPlayer.json',
      JSON.stringify(mostDismissedPlayer, null, 2),
    );
    console.log(
      '8- Most dismissed player by a player written to file mostDismissedPlayer.json',
    );

    // task-9: Find the bowler with the best economy in super overs
    const mostEconomicSuperOverBowler = findMostEconomicBowler(deliveriesData);
    fs.writeFileSync(
      './public/output/mostEconomicSuperOversBowler.json',
      JSON.stringify(mostEconomicSuperOverBowler, null, 2),
    );
    console.log(
      '9- Most economic superovers bowler written to file mostEconomicSuperOversBowler.json',
    );
  } catch (error) {
    console.error('An error occurred:', error);
  }
}

// Run all tasks
runTasks();
