const findPlayerWithMostManOfmatchAwards = require('../src/server/6-find-player-with-most-man-of-match-awards');

// Test Data
const matchesData = [
  { season: '2015', player_of_match: 'Player1' },
  { season: '2015', player_of_match: 'Player2' },
  { season: '2015', player_of_match: 'Player4' },
  { season: '2015', player_of_match: 'Player4' },
  { season: '2015', player_of_match: 'Player2' },
  { season: '2015', player_of_match: 'Player1' },
  { season: '2016', player_of_match: 'Player2' },
  { season: '2016', player_of_match: 'Player2' },
  { season: '2016', player_of_match: 'Player3' },
  { season: '2017', player_of_match: 'Player1' },
  { season: '2017', player_of_match: 'Player3' },
];

// Expected Result
const expectedData = {
  2015: ['Player1', 'Player2', 'Player4'],
  2016: ['Player2'],
  2017: ['Player1', 'Player3'],
};

// Jest Test
test('Find player with most Man of the Match awards by season', () => {
  expect(findPlayerWithMostManOfmatchAwards(matchesData)).toStrictEqual(
    expectedData,
  );
});
