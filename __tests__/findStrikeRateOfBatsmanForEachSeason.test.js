const calculateStrikeRate = require('../src/server/7-find-strike-rate-of-a-batsman-each-season');

const matchesData = [
  { id: 1, season: '2015' },
  { id: 2, season: '2015' },
  { id: 3, season: '2016' },
];

const deliveriesData = [
  { match_id: 1, batsman: 'Batsman1', total_runs: '20', ball: '10' },
  { match_id: 1, batsman: 'Batsman2', total_runs: '30', ball: '15' },
  { match_id: 2, batsman: 'Batsman1', total_runs: '40', ball: '20' },
  { match_id: 3, batsman: 'Batsman1', total_runs: '50', ball: '25' },
];

const expectedData = {
  2015: {
    Batsman1: { runs: 60, balls: 30, strikeRate: '200.00' },
    Batsman2: { runs: 30, balls: 15, strikeRate: '200.00' },
  },
  2016: {
    Batsman1: { runs: 50, balls: 25, strikeRate: '200.00' },
  },
};

test('Calculate strike rate for each batsman by season', () => {
  expect(calculateStrikeRate(matchesData, deliveriesData)).toEqual(
    expectedData,
  );
});
