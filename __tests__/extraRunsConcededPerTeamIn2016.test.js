const matchesWonPerTeamPerYear = require('../src/server/3-extra-runs-conceded-per-team-year-2016');

const matchesData = [
  { id: 1, season: '2018' },
  { id: 2, season: '2018' },
  { id: 3, season: '2019' },
  { id: 4, season: '2019' },
  { id: 4, season: '2019' },
  { id: 5, season: '2020' },
  { id: 6, season: '2020' },
];

const deliveriesData = [
  { match_id: 1, bowling_team: 'Rising Super Giants', extra_runs: '6' },
  { match_id: 1, bowling_team: 'Mumbai Indians', extra_runs: '2' },
  { match_id: 2, bowling_team: 'Kolkata Knight Riders', extra_runs: '13' },
  { match_id: 2, bowling_team: 'Chennai Superkings', extra_runs: '9' },
  { match_id: 1, bowling_team: 'Mumbai Indians', extra_runs: '12' },
  { match_id: 2, bowling_team: 'Kolkata Knight Riders', extra_runs: '9' },
  { match_id: 3, bowling_team: 'Pune Warriors', extra_runs: '8' },
  { match_id: 1, bowling_team: 'Rising Super Giants', extra_runs: '5' },
  { match_id: 3, bowling_team: 'Delhi Daredevils', extra_runs: '11' },
  { match_id: 2, bowling_team: 'Kolkata Knight Riders', extra_runs: '4' },
];

const targetYear = '2018';

test('Extra runs conceded per team in year 2018 is: ', () => {
  expect(
    matchesWonPerTeamPerYear(matchesData, deliveriesData, targetYear),
  ).toStrictEqual({
    'Chennai Superkings': 9,
    'Kolkata Knight Riders': 26,
    'Mumbai Indians': 14,
    'Rising Super Giants': 11,
  });
});
