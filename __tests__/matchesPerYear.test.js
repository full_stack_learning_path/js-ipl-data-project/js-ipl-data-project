const matchesPerYear = require('../src/server/1-matches-per-year');

const testData = [
  { season: 2008 },
  { season: 2008 },
  { season: 2008 },
  { season: 2008 },
  { season: 2009 },
  { season: 2009 },
  { season: 2009 },
  { season: 2009 },
  { season: 2010 },
  { season: 2010 },
  { season: 2010 },
];

test('matches in year 2008 to equal 4, 2009 to equal 4 and 2010 to equal 3 ', () => {
  expect(matchesPerYear(testData)).toStrictEqual({ 2008: 4, 2009: 4, 2010: 3 });
});
