const findMostDismissedPlayer = require('../src/server/8-find-most-dismissed-player-by-a-player');

const deliveries = [
  {
    batsman: 'DA Warner',
    bowler: 'Harbhajan Singh',
    dismissal_kind: 'caught',
  },
  {
    batsman: 'Virat Kohli',
    bowler: 'Michael Stark',
    dismissal_kind: 'caught',
  },
  {
    batsman: 'Virat Kohli',
    bowler: 'Michael Stark',
    dismissal_kind: 'caught',
  },
  {
    batsman: 'DA Warner',
    bowler: 'Harbhajan Singh',
    dismissal_kind: 'caught',
  },
  {
    batsman: 'MC Henriques',
    bowler: 'Sandeep Sharma',
    dismissal_kind: 'lbw',
  },
  {
    batsman: 'Virat Kohli',
    bowler: 'Michael Stark',
    dismissal_kind: 'bowled',
  },
];

const expected = {
  batsman: 'Virat Kohli',
  maxDismissalsByBowler: ['Michael Stark'],
  maxDismissals: 3,
};

const output = findMostDismissedPlayer(deliveries);

test('Calculate most dismissed player by a player in ipl history', () => {
  expect(output.toStrictEqual(expected));
});

test('highestDismissedByAnotherPlayer should return an object', () => {
  expect(typeof output).toBe('object');
});

test('highestDismissedByAnotherPlayer should contain dismissals by bowler', () => {
  expect(output).toStrictEqual(expected);
});

test('if there is no dismissals', () => {
  const answer = findMostDismissedPlayer([]);

  expect(answer).toStrictEqual({
    batsman: null,
    maxDismissalsByBowler: [],
    maxDismissals: 0,
  });
});
