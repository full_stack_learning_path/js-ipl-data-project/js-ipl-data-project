const mostEconomicBowlers = require('../src/server/4-top10-economical-bowlers-year-2015');

const matchesData = [
  { id: 1, season: '2015' },
  { id: 2, season: '2015' },
  { id: 3, season: '2015' },
  { id: 2, season: '2015' },
  { id: 1, season: '2015' },
  { id: 3, season: '2016' },
  { id: 4, season: '2017' },
  { id: 5, season: '2018' },
];

const deliveriesData = [
  { match_id: 1, bowler: 'Bowler1', total_runs: '2' },
  { match_id: 2, bowler: 'Bowler2', total_runs: '1' },
  { match_id: 2, bowler: 'Bowler3', total_runs: '6' },
  { match_id: 3, bowler: 'Bowler4', total_runs: '1' },
  { match_id: 2, bowler: 'Bowler5', total_runs: '6' },
  { match_id: 2, bowler: 'Bowler6', total_runs: '4' },
  { match_id: 4, bowler: 'Bowler7', total_runs: '3' },
  { match_id: 1, bowler: 'Bowler1', total_runs: '2' },
  { match_id: 2, bowler: 'Bowler2', total_runs: '2' },
  { match_id: 2, bowler: 'Bowler3', total_runs: '4' },
  { match_id: 3, bowler: 'Bowler4', total_runs: '1' },
  { match_id: 2, bowler: 'Bowler5', total_runs: '6' },
  { match_id: 2, bowler: 'Bowler6', total_runs: '4' },
  { match_id: 4, bowler: 'Bowler7', total_runs: '3' },
];

const targetYear = '2015';
const numberOfBowlers = 2;

test('Top 1 most economical bowler in year 2015 is: ', () => {
  expect(
    mostEconomicBowlers(
      matchesData,
      deliveriesData,
      targetYear,
      numberOfBowlers,
    ),
  ).toStrictEqual({
    Bowler2: {
      balls: 2,
      economy: '9.00',
      runs: 3,
    },
    Bowler4: {
      balls: 2,
      economy: '6.00',
      runs: 2,
    },
  });
});
