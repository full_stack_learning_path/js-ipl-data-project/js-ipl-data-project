const findMostEconomicalSuperoversBowler = require('../src/server/9-find-most-economic-super-overs-bowler');

const deliveriesData = [
  {
    match_id: 1,
    bowler: 'Bowler1',
    total_runs: '2',
    extra_runs: '0',
    is_super_over: '1',
  },
  {
    match_id: 1,
    bowler: 'Bowler1',
    total_runs: '4',
    extra_runs: '0',
    is_super_over: '1',
  },
  {
    match_id: 1,
    bowler: 'Bowler1',
    total_runs: '3',
    extra_runs: '0',
    is_super_over: '1',
  },
  {
    match_id: 2,
    bowler: 'Bowler2',
    total_runs: '1',
    extra_runs: '1',
    is_super_over: '1',
  },
  {
    match_id: 2,
    bowler: 'Bowler2',
    total_runs: '4',
    extra_runs: '0',
    is_super_over: '1',
  },
  {
    match_id: 2,
    bowler: 'Bowler2',
    total_runs: '3',
    extra_runs: '0',
    is_super_over: '1',
  },
  {
    match_id: 2,
    bowler: 'Bowler3',
    total_runs: '6',
    extra_runs: '1',
    is_super_over: '1',
  },
  {
    match_id: 2,
    bowler: 'Bowler3',
    total_runs: '4',
    extra_runs: '0',
    is_super_over: '1',
  },
  {
    match_id: 2,
    bowler: 'Bowler3',
    total_runs: '6',
    extra_runs: '0',
    is_super_over: '1',
  },
];

// console.log(findMostEconomicalSuperoversBowler(deliveriesData));

test('Calculate strike rate for each batsman by season', () => {
  expect(findMostEconomicalSuperoversBowler(deliveriesData)).toEqual({
    bowler: ['Bowler1', 'Bowler2'],
    economy: 18.0,
  });
});
