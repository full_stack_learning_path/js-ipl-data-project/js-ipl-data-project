const teamsWhoWonTossAndMatchBoth = require('../src/server/5-find-teams-who-won-toss-won-match');

const testData = [
  { toss_winner: 'TeamA', winner: 'TeamA' },
  { toss_winner: 'TeamB', winner: 'TeamB' },
  { toss_winner: 'TeamA', winner: 'TeamB' },
  { toss_winner: 'TeamC', winner: 'TeamC' },
  { toss_winner: 'TeamD', winner: 'TeamE' },
  { toss_winner: 'TeamE', winner: 'TeamE' },
];

test('Teams who won both toss as well match also tobe ', () => {
  expect(teamsWhoWonTossAndMatchBoth(testData)).toStrictEqual({
    TeamA: 1,
    TeamB: 1,
    TeamC: 1,
    TeamE: 1,
  });
});
