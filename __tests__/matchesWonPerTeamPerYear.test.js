const matchesWonPerTeamPerYear = require('../src/server/2-matches-won-per-team-per-year');

const testData = [
  { season: 2008, winner: 'Kings XI Punjab' },
  { season: 2008, winner: 'Mumbai Indians' },
  { season: 2008, winner: 'Delhi Daredevils' },
  { season: 2008, winner: 'Kolkata Knight Riders' },
  { season: 2009, winner: 'Kings XI Punjab' },
  { season: 2009, winner: 'Mumbai Indians' },
  { season: 2009, winner: 'Delhi Daredevils' },
  { season: 2009, winner: 'Kolkata Knight Riders' },
  { season: 2010, winner: 'Kings XI Punjab' },
  { season: 2010, winner: 'Mumbai Indians' },
  { season: 2010, winner: 'Delhi Daredevils' },
  { season: 2010, winner: 'Kolkata Knight Riders' },
];

test('matches won per team in year 2008 to equal 1 for every team and in year 2009 to equal 1 for every team', () => {
  expect(matchesWonPerTeamPerYear(testData)).toStrictEqual({
    2008: {
      'Kings XI Punjab': 1,
      'Mumbai Indians': 1,
      'Delhi Daredevils': 1,
      'Kolkata Knight Riders': 1,
    },
    2009: {
      'Kings XI Punjab': 1,
      'Mumbai Indians': 1,
      'Delhi Daredevils': 1,
      'Kolkata Knight Riders': 1,
    },
    2010: {
      'Kings XI Punjab': 1,
      'Mumbai Indians': 1,
      'Delhi Daredevils': 1,
      'Kolkata Knight Riders': 1,
    },
  });
});
